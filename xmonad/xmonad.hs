--------------------------------------------------------------------------------
-- | xmonad.hs
--
-- XMonad setup for my workstation.
--------------------------------------------------------------------------------
-- TODO: Protect against accidental presses of C-q in Firefox
-- TODO: Proper divisions in workspaces
-- TODO: Manage urgent windows (only the hook is installed)
-- TODO: Automatically place windows in pre-defined workspaces
-- TODO: Solarized color scheme
-- XXX: Use XMonad.Util.SpawnOnce for programs started at the beginning?
--------------------------------------------------------------------------------
module Main (main) where

--------------------------------------------------------------------------------
import System.Exit
import XMonad
import XMonad.Config.Desktop
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.BinarySpacePartition (emptyBSP)
import XMonad.Layout.NoBorders (noBorders)
import XMonad.Layout.ResizableTile (ResizableTall(..))
import XMonad.Layout.ToggleLayouts (ToggleLayout(..), toggleLayouts)
import XMonad.Prompt
import XMonad.Prompt.ConfirmPrompt
import XMonad.Prompt.Shell
import XMonad.Prompt.Window
import XMonad.Prompt.Workspace
import XMonad.Prompt.XMonad
import System.Taffybar.Hooks.PagerHints (pagerHints)
import XMonad.Util.EZConfig
import XMonad.Util.Run
import XMonad.Util.SpawnOnce

--------------------------------------------------------------------------------
main = do
  -- Start xmonad using the main desktop configuration with a few
  -- simple overrides:
  xmonad $ withUrgencyHook NoUrgencyHook
    $ ewmh $ pagerHints
    $ desktopConfig
    { modMask    = mod4Mask -- Use the "Win" key for the mod key
    , terminal   = "alacritty"
    , workspaces = [ "sys"
                   , "mail"
                   , "org"
                   , "chat"
                   , "web"
                   , "dev"
                   , "admin"
                   ]
    , manageHook = myManageHook <+> manageHook desktopConfig
    , layoutHook = desktopLayoutModifiers $ myLayouts
    , logHook    = dynamicLogString def >>= xmonadPropLog
    , startupHook = do
        spawnOnce "xscreensaver -no-splash"
        spawnOnce "xmodmap -e pointer = 3 2 1"
        spawnOnce "taffybar"
        spawnOnce "redshift-gtk"
        spawnOnce "pasystray"
    }

    `additionalKeysP` -- Add some extra key bindings:
    [ ("M-S-q",   confirmPrompt myXPConfig "exit" (io exitSuccess))
    , ("M-.",     shellPrompt myXPConfig)
    , ("M-,",     xmonadPrompt myXPConfig)
    , ("M-<Esc>", sendMessage (Toggle "Full"))
    -- Windows
    , ("M-g",     windowPrompt myXPConfig Goto wsWindows)
    -- Workspaces
    --, ("M-S-m",   workspacePrompt myXPConfig (windows . W.view))
    -- Apps
    , ("M-e", safeSpawn "emacsclient" ["-c", "-a", ""])
    , ("M-c", safeSpawn "alacritty" [])
    , ("M-C-<Delete>", safeSpawn "xscreensaver-command" ["--lock"])
    -- Audio
    , ("<XF86AudioLowerVolume>", safeSpawn "pactl" ["set-sink-volume", "@DEFAULT_SINK@", "-5%"])
    , ("<XF86AudioRaiseVolume>", safeSpawn "pactl" ["set-sink-volume", "@DEFAULT_SINK@", "+5%"])
    ]

--------------------------------------------------------------------------------
-- | Customize layouts.
--
-- This layout configuration uses two primary layouts, 'ResizableTall'
-- and 'BinarySpacePartition'.  You can also use the 'M-<Esc>' key
-- binding defined above to toggle between the current layout and a
-- full screen layout.
myLayouts = toggleLayouts (noBorders Full) others
  where
    others = ResizableTall 1 (1.5/100) (3/5) [] ||| emptyBSP

--------------------------------------------------------------------------------
-- | Customize the way 'XMonad.Prompt' looks and behaves.  It's a
-- great replacement for dzen.
myXPConfig = def
  { position          = Top
  , alwaysHighlight   = True
  , promptBorderWidth = 0
  , font              = "xft:Hack:size=12"
  }

--------------------------------------------------------------------------------
-- | Manipulate windows as they are created.  The list given to
-- @composeOne@ is processed from top to bottom.  The first matching
-- rule wins.
--
-- Use the `xprop' tool to get the info you need for these matches.
-- For className, use the second value that xprop gives you.
myManageHook = composeOne
  [ className =? "Pidgin" -?> doFloat
  , className =? "XCalc"  -?> doFloat
  , className =? "mpv"    -?> doFloat
  , isDialog              -?> doCenterFloat

    -- Move transient windows to their parent:
  , transience
  ]
