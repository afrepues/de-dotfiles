--------------------------------------------------------------------------------
-- | taffybar.hs
--
-- Status bar configuration.
--
-- @targetVersion 0.4.6
--------------------------------------------------------------------------------
-- TODO: Unicode 1-char labels for memory, CPU and time
-- TODO: Easier to use Solarized color scheme
-- TODO: Full height for the monitoring icons
--------------------------------------------------------------------------------
module Main where

import Text.Printf ( printf )
import System.Information.CPU
import System.Taffybar
import System.Taffybar.FreedesktopNotifications
import System.Taffybar.Pager
import System.Taffybar.SimpleClock
import System.Taffybar.Systray
import System.Taffybar.TaffyPager
import System.Taffybar.Text.MemoryMonitor
import System.Taffybar.Widgets.PollingGraph
import System.Taffybar.Widgets.PollingLabel ( pollingLabelNew )
import qualified Graphics.UI.Gtk as Gtk

cpuCallback = do
  (userLoad, systemLoad, totalLoad) <- cpuLoad
  return [totalLoad, systemLoad]

textualCpuMonitorNew :: IO Gtk.Widget
textualCpuMonitorNew = do
  label <- pollingLabelNew fmt period callback
  Gtk.widgetShowAll label
  return label
  where
    fmt = (colorize "#eee8d5" "#268bd2" " ⚙ ") ++ (colorize "#268bd2" "" " %6.2f%% ")
    period = 0.5
    callback = do
      (userLoad, systemLoad, totalLoad) <- cpuLoad
      return $ printf fmt (totalLoad * 100)

main = do
  let clock = textClockNew Nothing  ((colorize "#eee8d5" "#2aa198" " 🕒 ") ++ (colorize "#2aa198" "" " %H:%M ")) 1
      pager = taffyPagerNew PagerConfig
        { activeWindow     = escape . shorten 60
        , activeLayout     = escape
        , activeWorkspace  = colorize "#b58900" "" . const "■"
        , hiddenWorkspace  = const "□"
        , emptyWorkspace   = const "□"
        , visibleWorkspace = const "□"
        , urgentWorkspace  = colorize "#dc322f" "#002b36" . const "□"
        , widgetSep        = " │ "
        }
      note = notifyAreaNew defaultNotificationConfig
      mem = textMemoryMonitorNew (colorize "#eee8d5" "#6c71c4" (" M ") ++ (colorize "#6c71c4" "" " $used$ / $total$ ")) 1
      cpu = textualCpuMonitorNew
      tray = systrayNew
  defaultTaffybar defaultTaffybarConfig { startWidgets = [ pager, note ]
                                        , endWidgets = [ tray, clock, mem, cpu ]
                                        }
